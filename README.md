# World Time Extended

This is my first stab at Flutter and Dart. The project is based off of NetNinjas Flutter
tutorial with a few tweaks and modifications.

# Getting Started

## Installing Flutter & Android Studio - Ubuntu 20.04
```
- sudo apt install openjdk-14-jdk
- sudo snap install flutter --classic
- sudo snap install android-studio --classic
- flutter doctor --android-licenses
- flutter doctor
```
 - Install Flutter & Dart plugins in Android Studio
    - Studio: Settings -> Language & Frameworks -> Flutter -> SDK
        -  Set to /home/{user}/snap/flutter/common/flutter

    - Studio: Project Settings -> Project -> Project SDK
        - Set to Android API

## Modifications

This version is an extension of the tutorial linked below and has the following additions:
- Uses current IP Address to determine the current location
- Initiial app load defaults to the local time
- Displays the day of the week at the location specified
- New Home button returns you to your current local time
- New Day and Night images from UnSplash
- New Info route / page
- New locations and flags added (alphabetized)
- Bug fixes for date offset and incorrect country flag
- Misc style changes
- Ability to launch browser for external links

## Links
- [NetNinjas Github Flutter Tutorial Code](https://github.com/iamshaunjp/flutter-beginners-tutorial)
- [NetNinjas YouTube Channel](https://www.youtube.com/channel/UCW5YeuERMmlnqo4oq8vwUpg)
- [WorldTimeAPI](https://worldtimeapi.org/)
- [UnSplash](https://unsplash.com)




