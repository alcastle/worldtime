import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {

  String location;  // location name for the UI
  String time;      // Time in the location
  String flag;      // a url to a flag asset icon
  String path;      // url path for timezone
  String weekDay;   // Day of the week (Monday, Tuesday, ...)
  String url;       // base url to get our info from
  bool isDaytime;   // true or false if daytime or not

  WorldTime({this.location, this.flag, this.path});

  Future<void> getTime([detectLocation]) async {

    try {
      if (detectLocation != null) {
        // Get the users current time based on their IP Address
        url = 'http://worldtimeapi.org/api/ip';
        print('detectLocation is null - getting IP address based info');
      } else {
        url = 'http://worldtimeapi.org/api/timezone/$path';
      }
      Response response = await get(url);

      if (response.statusCode == 200) {
        Map data = jsonDecode(response.body);
        String datetime = data['datetime'];
        String offset = data['utc_offset'].substring(1, 3);
        String operator = data['utc_offset'].substring(0, 1);

        // create datetime object
        DateTime now = DateTime.parse(datetime);
        if (operator == '-') {
          now = now.subtract(Duration(hours: int.parse(offset)));
        } else {
          now = now.add(Duration(hours: int.parse(offset)));
        }

        // Is it day or night
        isDaytime = now.hour > 6 && now.hour < 20 ? true : false;

        // set the time property
        time = DateFormat.jm().format(now);

        // Get the weekday name
        weekDay = getWeekDayName(data['day_of_week']);

      } else {
        // Setting these prevents errors in home.dart
        isDaytime = false;
        time = 'Network Error';
      }
    } catch (e) {
      // Setting this so that the UI doesn't do red screen of death
      time  = 'Network Error';
      isDaytime = false;
    }
  }
}

String getWeekDayName(int dayOfWeek) {
  String day;
  switch (dayOfWeek) {
    case 1:
      day = 'Monday';
      break;
    case 2:
      day = 'Tuesday';
      break;
    case 3:
      day = 'Wednesday';
      break;
    case 4:
      day = 'Thursday';
      break;
    case 5:
      day = 'Friday';
      break;
    case 6:
      day = 'Saturday';
      break;
    case 7:
      day = 'Sunday';
      break;
  }

  return day;
}