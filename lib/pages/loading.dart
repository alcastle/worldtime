import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {

  // function to instantiate a new WorldTime object
  // If you use a await inside a function means that
  // the function also needs to be async
  void setupWorldTime() async {
    WorldTime instance = WorldTime(
      //location: 'Germany',
      //flag: 'germany.png',
      //path: 'Europe/Berlin',
    );

    // Calling world_time.dart getTime method
    await instance.getTime(true);

    // Redirect to the home screen
    Navigator.pushReplacementNamed(context, '/home', arguments: {
      // Seems like these are url parameters
      'location': instance.location,
      'flag': instance.flag,
      'time': instance.time,
      'isDaytime': instance.isDaytime,
      'weekDay': instance.weekDay,
    });
  }

  // void getData() async {
  //   Response response = await get('https://jsonplaceholder.typicode.com/todos/1');
  //   // Below requires the dart:convert import
  //   Map data = jsonDecode(response.body);
  //   print(data['title']);
  // }

  // void getData() async {
  //   // Simulate a network request for a username
  //   String username = await Future.delayed(Duration(seconds: 3), () {
  //     print('delay over');
  //     return 'castle';
  //   });
  //
  //   // Secondary delay that requires data from the above delay - dependency
  //   String bio = await Future.delayed(Duration(seconds: 3), () {
  //     print('second delay over');
  //     return 'super hero';
  //   });
  //
  //   // This depends upon the return values of both of the above
  //   print('$username - $bio');
  // }

  @override
  void initState() {
    super.initState();
    setupWorldTime();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[900],
      body: Center(
        child: SpinKitFadingFour(
          color: Colors.white,
          size: 80.0,
        ),
      ),
    );
  }
}

// http package
// web requests
// json response body
// convert a string to an integer