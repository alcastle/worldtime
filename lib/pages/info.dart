import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


class Info extends StatelessWidget {
  final String github       = 'https://github.com/iamshaunjp/flutter-beginners-tutorial';
  final String youtube      = 'https://www.youtube.com/channel/UCW5YeuERMmlnqo4oq8vwUpg';
  final String myGitLab     = 'https://gitlab.com/alcastle';
  final String unsplash     = 'https://unsplash.com';
  final String worldtimeapi = 'https://worldtimeapi.org/';
  final Color textColor     = Colors.white70;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: AppBar(
          backgroundColor: Colors.blue[900],
          title: Text(
            'INFO'.toUpperCase(),
            style: TextStyle(
              letterSpacing: 2,
              fontSize: 18,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
            )
          ),
          centerTitle: true,
          elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
              children: [
                Image.asset(
                  'assets/day.jpg',
                  width: 600,
                  height: 120,
                  fit: BoxFit.cover,
                ),
                SizedBox(height:20),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: Center(
                      child: Text(
                        'World Time Project',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            color: textColor
                        ),
                      ),
                    ),
                  ),
                ),
                textSection,
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Based on the most excellent Flutter tutorial by'
                              ' the NetNinja iamshaunjp',
                              style: TextStyle(
                                color: textColor,
                                fontSize: 18
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Icon(Icons.star,color: Colors.red[500]),
                    Text(
                      '100%',
                      style: TextStyle(
                        color: textColor,
                        fontSize: 14,
                      ),
                    ),
                ]
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  Expanded(
                    child: Icon(Icons.code, color: textColor),
                    flex: 1
                  ),
                  Expanded(
                    child:
                      InkWell(
                          child: Text(
                              'Tutorial GitHub Repo',
                              style: TextStyle(
                                fontSize: 18,
                                color: textColor,
                              ),
                          ),
                          onTap: () => openLink(github),

                      ),
                    flex: 10,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Icon(Icons.video_call, color: textColor),
                    flex: 1,
                  ),
                  Expanded(
                    child: InkWell(
                        child: Text(
                            'NetNinja Youtube Channel',
                            style: TextStyle(
                              fontSize: 18,
                              color: textColor,
                            ),
                        ),
                      onTap: () => openLink(youtube),
                    ),
                    flex: 10,
                  ),
                ],
              ),
              SizedBox(height: 25),
              const Divider(
                color: Colors.white70,
                height: 10,
                thickness: 1,
                indent: 5,
                endIndent: 5,
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: InkWell(
                      child: Text(
                        'Other links to checkout:\n',
                        style: TextStyle(
                            color: textColor,
                            fontSize: 18
                        ),
                      ),
                      onTap: () => openLink(unsplash),
                    ),
                    flex: 10,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Icon(Icons.image, color: textColor),
                    flex: 1,
                  ),
                  Expanded(
                    child: InkWell(
                      child: Text(
                        'Unsplash - Free Images',
                        style: TextStyle(
                          fontSize: 18,
                          color: textColor,
                        ),
                      ),
                      onTap: () => openLink(unsplash),
                    ),
                    flex: 10,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Icon(Icons.timer, color: textColor),
                    flex: 1,
                  ),
                  Expanded(
                    child: InkWell(
                      child: Text(
                        'WorldTime API',
                        style: TextStyle(
                          fontSize: 18,
                          color: textColor,
                        ),
                      ),
                      onTap: () => openLink(worldtimeapi),
                    ),
                    flex: 10,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Icon(Icons.open_in_browser, color: textColor),
                    flex: 1,
                  ),
                  Expanded(
                    child: InkWell(
                      child: Text(
                        'My GitLab Account',
                        style: TextStyle(
                          fontSize: 18,
                          color: textColor,
                        ),
                      ),
                      onTap: () => openLink(myGitLab),
                    ),
                    flex: 10,
                  ),
                ],
              ),
              SizedBox(height: 50),
            ]
          ),
        )
    );
  }
}

openLink(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  }
}

Widget textSection = Container(
  padding: const EdgeInsets.all(8),
  child: Text(
    'This version is an extension of the tutorial linked below'
    ' and has the following additions:\n'
    '\n - Uses current IP to determine location'
    '\n - Initial load defaults to the local time'
    '\n - Displays day of the week at location'
    '\n - New Home button returns to local time'
    '\n - New Day & Night images from Unsplash'
    '\n - New Info page'
    '\n - New locations added'
    '\n - Bug fixes for date offset & country flag'
    '\n - Misc style changes',
    softWrap: true,
    style: TextStyle(
      fontSize: 18,
      color: Colors.white70,
    ),
  ),
);

