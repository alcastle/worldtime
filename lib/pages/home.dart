import 'package:flutter/material.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Map params = {};

  @override
  Widget build(BuildContext context) {

    params = params.isNotEmpty ? params : ModalRoute.of(context).settings.arguments;

    String bgImage  = params['isDaytime'] ? 'day.jpg' : 'night.jpg';
    Color bgColor   = params['isDaytime'] ? Colors.blue : Colors.indigo[700];
    Color homeColor = params['isDaytime'] ? Colors.white70 : Colors.amber;
    String weekDay  = params['weekDay'] ?? 'Today';

    // If there's a network error - present it more gracefully
    String currentTime;
    double timeFontSize = 66;
    if (params['time'] == 'Network Error') {
      timeFontSize = 12;
      currentTime = 'Network Error\nCheck your internet connection';
    } else {
      currentTime = params['time'];
    }

    return Scaffold(
      backgroundColor: bgColor,
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/$bgImage'),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0,165,0,0),
            child: Column(
              children: [
                FlatButton.icon(
                    onPressed: () async {
                      dynamic result = await Navigator.pushNamed(context, '/location');
                      setState(() {
                        params = {
                          'time': result['time'] ?? null,
                          'flag': result['flag'] ?? null,
                          'isDaytime': result['isDaytime'] ?? null,
                          'location': result['location'] ?? null,
                          'weekDay': result['weekDay'] ?? null,
                        };
                      });
                    },
                    icon: Icon(
                      Icons.edit_location,
                      color: Colors.grey[300],
                    ),
                    label: Text(
                        'CHANGE LOCATION',
                        style: TextStyle(
                          letterSpacing: 2,
                          color: Colors.grey[300],
                        )
                    )
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      params['location'] ?? 'Current Location',
                      style: TextStyle(
                        fontSize: 28,
                        letterSpacing: 2,
                        color: Colors.white
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  weekDay.toUpperCase(),
                  style: TextStyle(
                    fontSize: 12,
                    letterSpacing: 4,
                    fontWeight: FontWeight.bold,
                    color: Colors.white70,
                  ),
                ),
                Text(
                  currentTime,
                  style: TextStyle(
                    fontSize: timeFontSize,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height:200),
                // Insert row with my time
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton.icon(
                        onPressed: () async {
                          await Navigator.pushNamed(context, '/');
                        },
                        icon: Icon(
                          Icons.home,
                          color: homeColor,
                        ),
                        label: Text(
                            'HOME',
                            style: TextStyle(
                              letterSpacing: 3,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: homeColor,
                            )
                        )
                    ),
                  ],
                ),
                SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0,50,0,0),
                      child: FlatButton.icon(
                          onPressed: () async {
                            await Navigator.pushNamed(context, '/info');
                          },
                          icon: Icon(
                            Icons.info_outline,
                            color: Colors.grey[600],
                          ),
                          label: Text(
                              'INFO',
                              style: TextStyle(
                                letterSpacing: 3,
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Colors.grey[600],
                              )
                          )
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
