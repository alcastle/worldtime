import 'package:flutter/material.dart';
import 'package:world_time/services/world_time.dart';

class ChooseLocation extends StatefulWidget {
  @override
  _ChooseLocationState createState() => _ChooseLocationState();
}


class _ChooseLocationState extends State<ChooseLocation> {

  List<WorldTime> locations = [
    WorldTime(path: 'Europe/Athens', location: 'Athens', flag: 'greece.png'),
    WorldTime(path: 'America/Belize', location:'Belize', flag: 'belize.png'),
    WorldTime(path: 'America/Argentina/Buenos_Aires', location:'Buenos Aires', flag: 'buenos_aires.png'),
    WorldTime(path: 'Africa/Cairo', location: 'Cairo', flag: 'egypt.png'),
    WorldTime(path: 'America/Chicago', location: 'Chicago', flag: 'usa.png'),
    WorldTime(path: 'Europe/Berlin', location: 'Germany', flag: 'germany.png'),
    WorldTime(path: 'Asia/Jakarta', location: 'Jakarta', flag: 'indonesia.png'),
    WorldTime(path: 'Europe/London', location: 'London', flag: 'uk.png'),
    WorldTime(path: 'America/Mexico_City', location:'Mexico City', flag:'mexico.png'),
    WorldTime(path: 'Africa/Nairobi', location: 'Nairobi', flag: 'kenya.png'),
    WorldTime(path: 'America/New_York', location: 'New York', flag: 'usa.png'),
    WorldTime(path: 'Asia/Seoul', location: 'Seoul', flag: 'south_korea.png'),
    WorldTime(path: 'Australia/Sydney', location:'Sydney', flag:'australia.png'),
  ];

  //
  void updateTime(index) async {
    WorldTime instance = locations[index];
    await instance.getTime();

    Navigator.pop(context, {
      'location': instance.location,
      'flag': instance.flag,
      'time': instance.time,
      'isDaytime': instance.isDaytime,
      'weekDay': instance.weekDay,
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text(
            'Choose Location'.toUpperCase(),
            style: TextStyle(
              letterSpacing: 2,
              fontSize: 14,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold
            )
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: ListView.builder(
          itemCount: locations.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(vertical: 1, horizontal: 4),
              child: Card(
                child: ListTile(
                  onTap: () {
                    updateTime(index);
                  },
                  title: Text(
                      locations[index].location,

                  ),
                  leading: CircleAvatar(
                      backgroundImage: AssetImage(
                        'assets/${locations[index].flag}'
                      ),
                  )
                ),
              ),
            );
          },
      ),
    );
  }
}
